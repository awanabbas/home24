# Home24 Artefacts
======

# Repository info
* https://bitbucket.org/awanabbas/home24

# About the app
The app is written in Swift which communicates with API from Home24 and shows the available articles in a swipe-able cards stack. After user has rated all articles, a review screen is presented to the user which shows all the items user has rated. The review screen can be seen in list and grid views. The items liked by user are marked in different way.

# Development Environment
* IDE: Xcode 9.4.1
* Languages: Swift 4.1
* Dependancy manager: Cocoapods 1.5.0

# Running the project
Make sure that you have compatible environment as listed above. Run pod install and open the workspace file in Xcode. The pods are pushed to the repository in case the Cocopads are not installed on reviewer's computer.

# Unit Tests
The project uses some frameworks for unit testing. The implementation can be updated and adapted/changed on need basis.

# Images
* Like Icon: https://www.flaticon.com/free-icon/like_196586 | (Flaticon Basic License) | Designed by author 'Roundicons' from Flaticon
* Dislike Icon: https://www.flaticon.com/free-icon/dislike_196587 | (Flaticon Basic License) | Designed by author 'Roundicons' from Flaticon
* Placeholder: Created by myself.
* Home24 Logo: Extracted from the website.
* Appicons: These are created by myself using the above logo.
* Company Icon : It is created by myself using the above logo.

# Third-party Libraries
* Alamofire: https://github.com/Alamofire/Alamofire | '4.7.2' | (MIT License)
* AlamofireObjectMapper: https://github.com/tristanhimmelman/AlamofireObjectMapper | '5.0.0' | (MIT License)
* Kingfisher: https://github.com/onevcat/Kingfisher | '4.8.0' | (MIT License)
* Koloda: https://github.com/Yalantis/Koloda | '4.3.1' | (MIT License)
* Mockingjay: https://github.com/kylef/Mockingjay | '2.0.1' | (BSD License)
* Quick: https://github.com/Quick/Quick | '1.3.0' | (Apache-2.0 License)
* Nimble: https://github.com/Quick/Nimble | '7.1.1' | (Apache-2.0 License)

# Contact
* Email: abbas.awan89@gmail.com
