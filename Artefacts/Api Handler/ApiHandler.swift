//
//  ApiHandler.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

typealias Completion = ([Article], Error?) -> ()

class ApiHandler {
    private struct Constants {
        static let apiEndpoint = "https://api-mobile.home24.com/api/v2.0"
        static let articlesApiPath = "/categories/100/articles"
        struct Parameters {
            static let appDomain = "appDomain"
            static let locale = "locale"
            static let limit = "limit"
            static let articlesAmount = "10"
        }
    }
    
    func fetchArtefacts(completion: @escaping Completion) {
        let requestUrl = self.articlesRequestURL()
        
        Alamofire.request(requestUrl).validate().responseObject { (response: DataResponse<ArticlesApiResponse>) in
            let articlesResponse = response.result.value
            completion(articlesResponse?.articles ?? [], response.error)
        }
    }
    
    // MARK: - Private functions
    private func articlesRequestURL() -> URL {
        let params = self.articlesParameterString()
        let path = Constants.articlesApiPath
        return self.requestURL(for: path, parameters: params)
    }
    
    private func requestURL(for apiPath: String, parameters: String) -> URL {
        let requestUrlString = Constants.apiEndpoint + apiPath + "?" + parameters
        return URL(string: requestUrlString)!
    }
    
    private func articlesParameterString() -> String {
        var params = Constants.Parameters.appDomain + "=" + "1"
        params = params + "&" + Constants.Parameters.locale + "=" + "de_DE"
        params = params + "&" + Constants.Parameters.limit + "=" + Constants.Parameters.articlesAmount

        return params
    }
}
