//
//  SplashViewController.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private functions
    private func setup() {
        // Do any setup/data fetching that is required for the app.
        // Then load home screen.
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.loadHomeScreen()
        }
    }
}
