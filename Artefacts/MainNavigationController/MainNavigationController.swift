//
//  MainNavigationController.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    // MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Private functions
    private func setup() {
        StyleManager.stylizeMainNavigationBar(self.navigationBar)
    }
}
