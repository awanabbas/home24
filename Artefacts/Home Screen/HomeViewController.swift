//
//  HomeViewController.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Properties
    @IBOutlet private weak var startButton: UIButton!
    
    // MARK: - Class functions
    class func newInstance() -> HomeViewController {
        let controller = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() as! HomeViewController
        
        return controller
    }
    
    // MARK: - Lifecycle functions
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        super.viewWillDisappear(animated)
    }
    
    // MARK: - IBActions
    @IBAction func startButtonTapped(_ sender: Any) {
        let controller = ArtefactBrowserViewController.newInstance()
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    // MARK: - Private functions
    private func setup() {
        self.title = "Home"
        self.startButton.setTitle("Start", for: .normal)
        self.titleLabel.text = "Browse our latest collection"
        self.navigationController?.isNavigationBarHidden = true

        self.customizeUI()
    }
    
    private func customizeUI() {
        self.view.backgroundColor = UIColor.home24GrayColor
        StyleManager.stylizeLabelWhite(self.titleLabel)
        StyleManager.stylizeButtonOrange(self.startButton)
    }
}
