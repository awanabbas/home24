//
//  ReviewListViewModel.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class ReviewListViewModel {
    
    // MARK: - Properties
    private let items: [ReviewCellModel]
    
    // MARK: - Lifecycle
    init(items: [ReviewCellModel]) {
        self.items = items
    }
    
    // MARK: - Public functions
    func numberOfItems() -> Int {
        return self.items.count
    }
    
    func getCellViewModel(at indexPath: IndexPath) -> ReviewCellModel {
        return self.items[indexPath.row]
    }
}
