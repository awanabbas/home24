//
//  ReviewCellModel.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class ReviewCellModel {
    let title: String
    let imageUrl: URL?
    let isLiked: Bool
    
    init(browserModel: ArtefactViewModel, isLiked: Bool = true) {
        self.title = browserModel.title
        self.imageUrl = browserModel.mediaUrl
        self.isLiked = isLiked
    }
}
