//
//  ArtefactViewModel.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

struct ArtefactViewModel {
    let title: String
    let sku: String
    let mediaUrl: URL?

    init?(article: Article) {
        guard let title = article.title else {
            return nil
        }
        
        guard let sku = article.sku else {
            return nil
        }
        
        guard let media = article.media else {
            return nil
        }
        
        self.title = title
        self.sku = sku
        self.mediaUrl = URL(string: media)
    }
}
