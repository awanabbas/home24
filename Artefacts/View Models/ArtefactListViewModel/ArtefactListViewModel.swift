//
//  ArtefactListViewModel.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class ArtefactListViewModel {
    
    // MARK: - Properties
    private var apiHandler: ApiHandler!
    var errorBlock: ((Error) -> Void)?
    var reloadListClosure: (() -> Void)?
    var hasFinishedLoadingData = false

    private var artefactViewModels: [ArtefactViewModel] = [] {
        didSet {
            self.reloadListClosure?()
        }
    }
    
    var numberOfCards: Int {
        return self.artefactViewModels.count
    }
    
    // MARK: - Lifecycle
    init() {
        self.apiHandler = ApiHandler()
    }
    
    // MARK: - Public functions
    func fetchArtefacts() {
        self.hasFinishedLoadingData = false

        self.apiHandler.fetchArtefacts { (list, error) in
            self.hasFinishedLoadingData = true
            
            if let anError = error {
                self.errorBlock?(anError)
            } else {
                self.handleUpdate(articles: list)
            }
        }
    }
    
    func getCardViewModel(at index: Int) -> ArtefactViewModel {
        return self.artefactViewModels[index]
    }
    
    // MARK: - Private functions
    private func handleUpdate(articles: [Article]) {
        var artefacts: [ArtefactViewModel] = []
        
        for anArticle in articles {
            if let artefact = ArtefactViewModel(article: anArticle) {
                artefacts.append(artefact)
            }
        }
        
        self.artefactViewModels = artefacts
    }
}
