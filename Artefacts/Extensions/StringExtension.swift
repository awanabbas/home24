//
//  StringExtension.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

extension String {
    func addKeyPath(path: String) -> String {
        guard path != "" else {
            return self
        }
        
        return self + "." + path
    }
}
