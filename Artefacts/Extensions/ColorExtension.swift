//
//  ColorExtension.swift
//  Artefacts
//
//  Created by Abbas Awan on 30/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

extension UIColor {
    static var home24OrangeColor: UIColor {
        return self.color(r: 244, g: 83, b: 52)
    }
    
    static var home24GrayColor: UIColor {
        return self.color(r: 53, g: 64, b: 68)
    }
    
    private class func color(r:CGFloat, g:CGFloat, b: CGFloat, a: CGFloat = 1.0) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
}
