//
//  ErrorPresenter.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

protocol ErrorPresenter: MessagePresenter {
    func showError(_ error: Error)
}

extension ErrorPresenter where Self: UIViewController {
    func showError(_ error: Error) {
        self.showMessage(title: "Error", message: error.localizedDescription)
    }
}
