//
//  CardView.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit
import Kingfisher

class CardView: UIView {
    
    // MARK: - Properties
    @IBOutlet private weak var imageView: UIImageView!
    
    // MARK: - Class functions
    class func newInstance() -> CardView {
        return UINib(nibName: "CardView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CardView
    }
    
    // MARK: - Public functions
    func render(url: URL?) {
        if let url = url {
            self.imageView.kf.setImage(with: url)
        } else {
            self.imageView.image = #imageLiteral(resourceName: "placeholder")
        }
    }
    
    // MARK: - Private functions
}
