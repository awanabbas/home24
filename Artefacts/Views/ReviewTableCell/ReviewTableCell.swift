//
//  ReviewTableCell.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit
import Kingfisher

class ReviewTableCell: UITableViewCell {

    // MARK: - Properties
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    var likeView: UIImageView!

    // MARK: - Lifecycle functions
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Public functions
    func render(viewModel: ReviewCellModel) {
        self.titleLabel.text = viewModel.title
        self.articleImageView.kf.setImage(with: viewModel.imageUrl)
        
        self.accessoryView = viewModel.isLiked ? self.likeView : nil
    }
    
    // MARK: - Private functions
    private func setup() {
        self.likeView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        self.likeView.image = #imageLiteral(resourceName: "like")
    }
}
