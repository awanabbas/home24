//
//  ReviewCollectionCell.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit
import Kingfisher

class ReviewCollectionCell: UICollectionViewCell {
    // MARK: - Properties
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var likeView: UIImageView!
    
    // MARK: - Lifecycle functions
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.setup()
    }
    
    // MARK: - Public functions
    func render(viewModel: ReviewCellModel) {
        self.imageView.kf.setImage(with: viewModel.imageUrl)
        
        self.likeView.isHidden = !viewModel.isLiked
    }
    
    // MARK: - Private functions
    private func setup() {
        self.likeView.image = #imageLiteral(resourceName: "like")
    }
}
