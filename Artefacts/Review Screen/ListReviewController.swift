//
//  ListReviewController.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class ListReviewController: UITableViewController {
    
    // MARK: - Constants
    private struct Constants {
        static let cellReuseIdentifier = "ListCellReuseIdentifier"
    }

    // MARK: - Properties
    private var viewModel: ReviewListViewModel!
    
    // MARK: - Class functions
    class func newInstance(viewModel: ReviewListViewModel) -> ListReviewController {
        let controller = UIStoryboard(name: "Review", bundle: nil).instantiateViewController(withIdentifier: "ListReviewController") as! ListReviewController
        controller.viewModel = viewModel
        
        return controller
    }
    
    // MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }

    // MARK: - Private functions
    private func setup() {
        let cellNib = UINib(nibName: "ReviewTableCell", bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: Constants.cellReuseIdentifier)
    }
}

// MARK: - TableView data source functions
extension ListReviewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellReuseIdentifier, for: indexPath) as! ReviewTableCell
        
        cell.render(viewModel: self.viewModel.getCellViewModel(at: indexPath))
        
        return cell
    }
}

// MARK: - TableView delegate functions
extension ListReviewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
