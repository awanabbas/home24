//
//  GridReviewController.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class GridReviewController: UICollectionViewController {
    
    // MARK: - Constants
    fileprivate struct Constants {
        static let reuseIdentifier = "CellReuseIdentifier"
    }
    
    // MARK: - Properties
    private var viewModel: ReviewListViewModel!

    // MARK: - Class functions
    class func newInstance(viewModel: ReviewListViewModel) -> GridReviewController {
        let controller = UIStoryboard(name: "Review", bundle: nil).instantiateViewController(withIdentifier: "GridReviewController") as! GridReviewController
        controller.viewModel = viewModel
        
        return controller
    }
    
    // MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Private functions
    private func setup() {
        let cellNib = UINib(nibName: "ReviewCollectionCell", bundle: nil)
        self.collectionView?.register(cellNib, forCellWithReuseIdentifier: Constants.reuseIdentifier)
        
        if let flowLayout = self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.itemSize = self.itemSize()
        }
    }
    
    private func itemSize() -> CGSize {
        // 2 items per column, with 5 points padding
        let width = (self.view.frame.width / 2) - 5
        return CGSize(width: width, height: width)
    }
}

// MARK: UICollectionView data source functions
extension GridReviewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.reuseIdentifier, for: indexPath) as! ReviewCollectionCell
        
        // Configure the cell
        cell.render(viewModel: self.viewModel.getCellViewModel(at: indexPath))
        
        return cell
    }
}

// MARK: UICollectionView delegate functions
extension GridReviewController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}
