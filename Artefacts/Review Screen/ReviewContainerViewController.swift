//
//  ReviewContainerViewController.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class ReviewContainerViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
    var items: [ReviewCellModel]!
    
    private var listReviewController: ListReviewController!
    private var gridReviewController: GridReviewController!
    
    // MARK: - Class functions
    class func newInstance(items: [ReviewCellModel]) -> ReviewContainerViewController {
        let controller = UIStoryboard(name: "Review", bundle: nil).instantiateInitialViewController() as! ReviewContainerViewController
        controller.items = items
        
        return controller
    }
    
    // MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    @IBAction func segmentedControlValueChanged(_ sender: Any) {
        self.updateResultView()
    }
    
    // MARK: - Private functions
    private func setup() {
        self.title = "Review"
        
        // The number of segments is configured in storyboard
        self.segmentedControl.setTitle("List", forSegmentAt: 0)
        self.segmentedControl.setTitle("Grid", forSegmentAt: 1)
        StyleManager.stylizeSegmentedControlOrange(self.segmentedControl)
        
        let viewModel = ReviewListViewModel(items: self.items)
        self.listReviewController = ListReviewController.newInstance(viewModel: viewModel)
        self.gridReviewController = GridReviewController.newInstance(viewModel: viewModel)
        
        self.updateResultView()
    }
    
    private func add(child viewController: UIViewController) {
        // Add passed view controller as child view controller
        addChildViewController(viewController)
        self.containerView.addSubview(viewController.view)
        viewController.view.frame = self.containerView.bounds
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(child viewController: UIViewController) {
        // Remove and notify child view controller
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
    
    private func updateResultView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            self.remove(child: self.gridReviewController)
            self.add(child: self.listReviewController)
        } else {
            self.remove(child: self.listReviewController)
            self.add(child: self.gridReviewController)
        }
    }

}
