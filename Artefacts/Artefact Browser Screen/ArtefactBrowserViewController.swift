//
//  ArtefactBrowserViewController.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit
import Koloda

class ArtefactBrowserViewController: UIViewController, ErrorPresenter {
    
    // MARK: - Properties
    @IBOutlet private weak var likeButton: UIButton!
    @IBOutlet private weak var dislikeButton: UIButton!
    @IBOutlet private weak var reviewButton: UIButton!
    @IBOutlet private weak var swipeView: KolodaView!
    @IBOutlet private weak var counterLabel: UILabel!
    @IBOutlet private weak var counterHeadingLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var likesCounter: Int = -1
    var reviewedItems: [ReviewCellModel] = []
    var viewModel: ArtefactListViewModel!

    // MARK: - Class functions
    class func newInstance() -> ArtefactBrowserViewController {
        let controller = UIStoryboard(name: "ArtefactBrowser", bundle: nil).instantiateInitialViewController() as! ArtefactBrowserViewController
        
        return controller
    }
    
    // MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.viewModel.hasFinishedLoadingData {
            self.messageLabel.isHidden = true
        }
        
        // Reset all the states
        self.resetState()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    @IBAction func likeButtonTapped(_ sender: Any) {
        self.swipeView.swipe(.right)
    }
    
    @IBAction func dislikeButtonTapped(_ sender: Any) {
        self.swipeView.swipe(.left)
    }
    
    @IBAction func reviewButtonTapped(_ sender: Any) {
        let controller = ReviewContainerViewController.newInstance(items: self.reviewedItems)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Private functions
    private func setup() {
        self.title = "Browse"
        self.counterHeadingLabel.text = "Likes:"
        self.reviewButton.setTitle("Review", for: .normal)
        self.updateMessage("Loading data...")
        
        self.setupViewModel()
        self.updateCounterVisiblity(isHidden: true)
        
        StyleManager.stylizeButtonOrange(self.reviewButton)
        StyleManager.stylizeLabelOrange(self.counterHeadingLabel)
        StyleManager.stylizeLabelGray(self.messageLabel)

        self.swipeView.dataSource = self
        self.swipeView.delegate = self
    }
    
    private func setupViewModel() {
        // Initialize view model
        self.viewModel = ArtefactListViewModel()
        
        // Bind view model
        self.viewModel.reloadListClosure = { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.messageLabel.isHidden = true
            self?.resetState()
        }
        
        self.viewModel.errorBlock = { [weak self] error in
            self?.activityIndicator.stopAnimating()
            self?.messageLabel.isHidden = false
            self?.updateCounterVisiblity(isHidden: true)
            self?.updateLikeDislikeButtons(enabled: false)
            self?.showError(error)
            self?.updateMessage("Something went wrong. Please try again")
        }
        
        // Fetch the list of Artefacts
        self.viewModel.fetchArtefacts()
    }
    
    private func resetState() {
        // The -1 will become 0 in the very next statement
        self.likesCounter = -1
        self.reviewedItems = []
        self.incrementLikesCounter()
        self.updateLikeDislikeButtons(enabled: true)
        self.updateCounterVisiblity(isHidden: false)
        self.updateReviewButton(enabled: false)
        self.swipeView.resetCurrentCardIndex()
    }
    
    private func updateCounterVisiblity(isHidden: Bool) {
        self.counterHeadingLabel.isHidden = isHidden
        self.counterLabel.isHidden = isHidden
    }
    
    private func updateLikeDislikeButtons(enabled: Bool) {
        self.likeButton.isEnabled = enabled
        self.dislikeButton.isEnabled = enabled
    }
    
    private func updateReviewButton(enabled: Bool) {
        self.reviewButton.isEnabled = enabled
        self.reviewButton.alpha = enabled ? 1.0 : 0.5
    }
    
    private func incrementLikesCounter() {
        func updateLikesLabel() {
            self.counterLabel.text = "\(self.likesCounter)" + "/" + "\(self.viewModel.numberOfCards)"
        }
        
        self.likesCounter = self.likesCounter + 1
        updateLikesLabel()
    }
    
    private func updateMessage(_ message: String) {
        self.messageLabel.text = message
    }
}

// MARK: - KolodaView data source functions
extension ArtefactBrowserViewController: KolodaViewDataSource {
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.viewModel.numberOfCards
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let view = CardView.newInstance()
        let viewModelAtIndex = self.viewModel.getCardViewModel(at: index)
        view.render(url: viewModelAtIndex.mediaUrl)
        
        return view
    }
}

// MARK: - KolodaView delegate functions
extension ArtefactBrowserViewController: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        self.updateReviewButton(enabled: true)
        self.updateLikeDislikeButtons(enabled: false)
        self.messageLabel.isHidden = false
        self.updateMessage("You have rated all items. Click on Review button to review.")
    }
    
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        if direction == .right {
            self.incrementLikesCounter()
        }
        
        let viewModelAtIndex = self.viewModel.getCardViewModel(at: index)
        self.addToReviewedList(item: viewModelAtIndex, isLiked: direction == .right)
    }
    
    private func addToReviewedList(item: ArtefactViewModel, isLiked: Bool) {
        let item = ReviewCellModel(browserModel: item, isLiked: isLiked)
        self.reviewedItems.append(item)
    }
}
