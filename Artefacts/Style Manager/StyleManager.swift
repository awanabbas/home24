//
//  StyleManager.swift
//  Artefacts
//
//  Created by Abbas Awan on 30/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class StyleManager {
    class func stylizeButtonOrange(_ button: UIButton) {
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.home24OrangeColor
        button.layer.cornerRadius = button.frame.height / 2
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
    class func stylizeSegmentedControlOrange(_ control: UISegmentedControl) {
        control.tintColor = UIColor.home24OrangeColor
    }
    
    class func stylizeMainNavigationBar(_ bar: UINavigationBar) {
        bar.barTintColor = UIColor.home24GrayColor
        bar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        bar.tintColor = UIColor.white
        bar.isTranslucent = false
    }
    
    class func stylizeLabelOrange(_ label: UILabel) {
        label.textColor = UIColor.home24OrangeColor
        label.font = UIFont.boldSystemFont(ofSize: 18)
    }
    
    class func stylizeLabelGray(_ label: UILabel) {
        label.textColor = UIColor.home24GrayColor
        label.font = UIFont.boldSystemFont(ofSize: 17)
    }
    
    class func stylizeLabelWhite(_ label: UILabel) {
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 17)
    }
}
