//
//  ArticlesApiResponse.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit
import ObjectMapper

class ArticlesApiResponse: Mappable {
    private struct Keys {
        static let embedded = "_embedded"
        static let articles = "articles"
    }
    
    var articles: [Article]?
    
    required init?(map: Map) {
        guard let embedded = map.JSON[Keys.embedded] as? [String : Any] else {
            return nil
        }
        
        guard let _ = embedded[Keys.articles] else {
            return nil
        }
        
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        articles <- map[Keys.embedded.addKeyPath(path: Keys.articles)]
    }
}
