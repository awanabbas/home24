//
//  Article.swift
//  Artefacts
//
//  Created by Abbas Awan on 29/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit
import ObjectMapper

class Article: Mappable {
    private struct Keys {
        static let title = "title"
        static let sku = "sku"
        static let description = "description"
        static let price = "price"
        static let amount = "amount"
        static let currency = "currency"
        static let brand = "brand"
        static let media = "media"
        static let uri = "uri"
        static let links = "_links"
        static let selfKey = "self"
        static let href = "href"
    }
    
    var title: String?
    var sku: String?
    var description: String?
    var price: String?
    var currency: String?
    var brand: String?
    var media: String?
    var link: String?

    required init?(map: Map) {
        guard let _ = map.JSON[Keys.sku] else {
            return nil
        }
        
        guard let _ = map.JSON[Keys.title] else {
            return nil
        }
        
        guard let price = map.JSON[Keys.price] as? [String : Any], let _ = price[Keys.amount], let _ = price[Keys.currency] else {
            return nil
        }
        
        guard let media = map.JSON[Keys.media] as? [[String : Any]], let firstMedia = media.first, let _ = firstMedia[Keys.uri] else {
            return nil
        }
        
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        sku <- map[Keys.sku]
        title <- map[Keys.title]
        description <- map[Keys.description]
        price <- map[Keys.price.addKeyPath(path: Keys.amount)]
        currency <- map[Keys.price.addKeyPath(path: Keys.currency)]
        brand <- map[Keys.brand.addKeyPath(path: Keys.title)]
        media <- map[Keys.media.addKeyPath(path: "0").addKeyPath(path: Keys.uri)]
        link <- map[Keys.links.addKeyPath(path: Keys.selfKey).addKeyPath(path: Keys.href)]
    }
    
}
