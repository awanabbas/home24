//
//  Json.swift
//  ArtefactsTests
//
//  Created by Abbas Awan on 30/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

typealias JSON = [String : Any]

enum JsonRepresentation {
    
    case object(_: JSON)
    case array(_: [JSON])
    
    init?(data: Any) {
        if let object = data as? JSON {
            self = .object(object)
            return
        }
        
        if let array = data as? [JSON] {
            self = .array(array)
            return
        }
        
        return nil
    }
}
