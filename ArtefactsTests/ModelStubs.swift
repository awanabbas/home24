//
//  ModelStubs.swift
//  ArtefactsTests
//
//  Created by Abbas Awan on 30/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import UIKit

class ModelStubs {
    class func getCorrectStubs() -> [JSON] {
        let artefact1: JSON = [
            "sku" : "000000001000091266",
            "title" : "Premium Komfortmatratze Smood",
            "description": NSNull(),
            "price" : [
                "amount" : "699.99",
                "currency" : "EUR",
                "recommendedRetailPrice": false
            ],
            "brand" : [
                "id" : "",
                "title" : "Smood",
                "logo" : ["/smood-1002018.gif"],
                "description" : ""
            ],
            "media" : [[
                "uri" : "https://cdn1.home24.net/resized/media/catalog/product/m/a/matratzenbezug-smood-webstoff-180-x-200cm-3477221.png?output-format=jpg&interpolation=progressive-bicubic",
                "mimeType" : "image/png",
                "type" : NSNull(),
                "priority" : 0,
                "size" : NSNull()
                ]],
            "_links" : [
                "webShopUrl" : NSNull(),
                "self" : [
                    "href" : "https://api-mobile.home24.com/api/v1/articles/000000001000091266?appDomain=1&locale=de_DE"
                ]
            ]
        ]
        
        let artefact2: JSON = [
            "sku" : "000000001000062031",
            "title" : "Boxspringbett Kinx",
            "description" : NSNull(),
            "price" : [
                "amount" : "1899.99",
                "currency" : "EUR",
                "recommendedRetailPrice" : false
            ],
            "brand" : [
                "id" : "",
                "title" : "KINX",
                "logo" : ["/kinx-1885934.gif"],
                "description": ""
            ],
            "media" : [[
                "uri" : "https://cdn1.home24.net/resized/media/catalog/product/-/1/-1000062031-180615-124843136-IMAGE-P000000001000062031.png?output-format=jpg&interpolation=progressive-bicubic",
                "mimeType": "image/png",
                "type" : NSNull(),
                "priority" : 0,
                "size" : NSNull()
                ]],
            "_links" : [
                "webShopUrl": NSNull(),
                "self" : [
                    "href" : "https://api-mobile.home24.com/api/v1/articles/000000001000062031?appDomain=1&locale=de_DE"
                ]
            ]
        ]
        
        return [artefact1, artefact2]
    }
    
    class func getIncorrectStubs() -> [JSON] {
        let artefact1: JSON = [ // Doesnt have "sku" field which is a required field
            "title" : "Premium Komfortmatratze Smood",
            "description": NSNull(),
            "price" : [
                "amount" : "699.99",
                "currency" : "EUR",
                "recommendedRetailPrice": false
            ],
            "brand" : [
                "id" : "",
                "title" : "Smood",
                "logo" : ["/smood-1002018.gif"],
                "description" : ""
            ],
            "media" : [[
                "uri" : "https://cdn1.home24.net/resized/media/catalog/product/m/a/matratzenbezug-smood-webstoff-180-x-200cm-3477221.png?output-format=jpg&interpolation=progressive-bicubic",
                "mimeType" : "image/png",
                "type" : NSNull(),
                "priority" : 0,
                "size" : NSNull()
                ]],
            "_links" : [
                "webShopUrl" : NSNull(),
                "self" : [
                    "href" : "https://api-mobile.home24.com/api/v1/articles/000000001000091266?appDomain=1&locale=de_DE"
                ]
            ]
        ]
        
        let artefact2: JSON = [ // Doesnt have "title" field which is a required field
            "sku" : "000000001000062031",
            "description" : NSNull(),
            "price" : [
                "amount" : "1899.99",
                "currency" : "EUR",
                "recommendedRetailPrice" : false
            ],
            "brand" : [
                "id" : "",
                "title" : "KINX",
                "logo" : ["/kinx-1885934.gif"],
                "description": ""
            ],
            "media" : [[
                "uri" : "https://cdn1.home24.net/resized/media/catalog/product/-/1/-1000062031-180615-124843136-IMAGE-P000000001000062031.png?output-format=jpg&interpolation=progressive-bicubic",
                "mimeType": "image/png",
                "type" : NSNull(),
                "priority" : 0,
                "size" : NSNull()
                ]],
            "_links" : [
                "webShopUrl": NSNull(),
                "self" : [
                    "href" : "https://api-mobile.home24.com/api/v1/articles/000000001000062031?appDomain=1&locale=de_DE"
                ]
            ]
        ]
        
        let artefact3: JSON = [ // Doesnt have "price" field which is a required field
            "sku" : "000000001000062031",
            "title" : "Boxspringbett Kinx",
            "description" : NSNull(),
            "brand" : [
                "id" : "",
                "title" : "KINX",
                "logo" : ["/kinx-1885934.gif"],
                "description": ""
            ],
            "media" : [[
                "uri" : "https://cdn1.home24.net/resized/media/catalog/product/-/1/-1000062031-180615-124843136-IMAGE-P000000001000062031.png?output-format=jpg&interpolation=progressive-bicubic",
                "mimeType": "image/png",
                "type" : NSNull(),
                "priority" : 0,
                "size" : NSNull()
                ]],
            "_links" : [
                "webShopUrl": NSNull(),
                "self" : [
                    "href" : "https://api-mobile.home24.com/api/v1/articles/000000001000062031?appDomain=1&locale=de_DE"
                ]
            ]
        ]
        
        let artefact4: JSON = [ // Doesnt have "media" field which is a required field
            "sku" : "000000001000091266",
            "title" : "Premium Komfortmatratze Smood",
            "description": NSNull(),
            "price" : [
                "amount" : "699.99",
                "currency" : "EUR",
                "recommendedRetailPrice": false
            ],
            "brand" : [
                "id" : "",
                "title" : "Smood",
                "logo" : ["/smood-1002018.gif"],
                "description" : ""
            ],
            "_links" : [
                "webShopUrl" : NSNull(),
                "self" : [
                    "href" : "https://api-mobile.home24.com/api/v1/articles/000000001000091266?appDomain=1&locale=de_DE"
                ]
            ]
        ]
        
        return [artefact1, artefact2, artefact3, artefact4]
    }

}
