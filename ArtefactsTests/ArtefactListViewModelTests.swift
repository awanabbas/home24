//
//  ArtefactListViewModelTests.swift
//  ArtefactsTests
//
//  Created by Abbas Awan on 30/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import XCTest
import Quick
import Nimble

@testable import Artefacts

class ArtefactListViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testViewModel() {
        let viewModel = ArtefactListViewModel()
        let timeout: TimeInterval = 30

        waitUntil(timeout: timeout) { done in
            viewModel.reloadListClosure = {
                expect(viewModel.numberOfCards).to(beGreaterThan(0), description: "Should have at least 1 Article")
                done()
            }
            
            viewModel.errorBlock = { error in
                // Should not have error
                XCTAssertNil(error, "Error should be nil")
                done()
            }
            
            viewModel.fetchArtefacts()
        }
    }
}
