//
//  ModelTests.swift
//  ArtefactsTests
//
//  Created by Abbas Awan on 30/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import Artefacts

class ModelTests: XCTestCase {
    var correctStubs: [JSON]!
    
    override func setUp() {
        super.setUp()
        self.correctStubs = ModelStubs.getCorrectStubs()
    }
    
    override func tearDown() {
        self.correctStubs = nil
        super.tearDown()
    }
    
    func testArticlesResponseMappingSuccess() {
        let artefactList: JSON = ["_embedded" : ["articles" : self.correctStubs]]
        let articlesListResponse = Mapper<ArticlesApiResponse>().map(JSON: artefactList)
        
        XCTAssertNotNil(articlesListResponse, "Mapping result should not be nil")
        guard let articles = articlesListResponse?.articles else {
            XCTFail("Articles should not be nil")
            return
        }
        
        XCTAssertGreaterThan(articles.count, 0, "There should be at least one Article")
    }
    
    func testArticlesResponseMappingFailure() {
        var artefactList: JSON = ["_embedded" : ["InvalidKey" : self.correctStubs]]
        var articlesListResponse = Mapper<ArticlesApiResponse>().map(JSON: artefactList)
        
        XCTAssertNil(articlesListResponse, "Mapping result should be nil")
        XCTAssertNil(articlesListResponse?.articles, "Articles should be nil")
        
        artefactList = ["InvalidKey" : ["articles" : self.correctStubs]]
        articlesListResponse = Mapper<ArticlesApiResponse>().map(JSON: artefactList)
        
        XCTAssertNil(articlesListResponse, "Mapping result should be nil")
        XCTAssertNil(articlesListResponse?.articles, "Articles should be nil")
    }
    
    func testArticleMappingSuccess() {
        for anItem in self.correctStubs {
            let articlesResponse = Mapper<Article>().map(JSON: anItem)
            XCTAssertNotNil(articlesResponse, "Mapping result should not be nil")
        }
    }
    
    func testArticleMappingFailure() {
        // Get list of incorrect stubs with multiple types of incorrect stubs
        let stubs = ModelStubs.getIncorrectStubs()
        for anItem in stubs {
            let articlesResponse = Mapper<Article>().map(JSON: anItem)
            XCTAssertNil(articlesResponse, "Mapping result should be nil")
        }
    }
}
