//
//  NetworkTests.swift
//  ArtefactsTests
//
//  Created by Abbas Awan on 30/7/2018.
//  Copyright © 2018 Abbas Awan. All rights reserved.
//

import XCTest
import Quick
import Nimble
import Mockingjay

@testable import Artefacts

class NetworkTests: QuickSpec {
    
    override func spec() {
        
        describe("Artefact List Request") {
            var apiHandler: ApiHandler!
            var stubs: [JSON]!
            
            // The timeout interval is short because we are using mocking data for
            // api calls
            let timeout: TimeInterval = 5

            beforeEach {
                apiHandler = ApiHandler()
                stubs = ModelStubs.getCorrectStubs()
            }
            
            context("Get the list") {
                it("Should return a list of Artefacts: Valid response") {
                    let artefactList: JSON = ["_embedded" : ["articles" : stubs]]
                    self.stub(everything, json(artefactList))
                    var apiResponse: [Article]? = nil
                    var networkError: Error? = nil
                    
                    apiHandler.fetchArtefacts(completion: { (list, error) in
                        apiResponse = list
                        networkError = error
                    })
                    
                    expect(apiResponse).toEventuallyNot(beNil(), timeout: timeout)
                    expect(apiResponse?.count).toEventually(beGreaterThan(0), timeout: timeout)
                    expect(networkError).toEventually(beNil(), timeout: timeout)
                }
                
                it("Should return an empty list: Empty response") {
                    let artefactList: JSON = ["_embedded" : ["articles" : []]]
                    self.stub(everything, json(artefactList))
                    var apiResponse: [Article]? = nil
                    var networkError: Error? = nil
                    
                    apiHandler.fetchArtefacts(completion: { (list, error) in
                        apiResponse = list
                        networkError = error
                    })
                    
                    expect(apiResponse).toEventuallyNot(beNil(), timeout: timeout)
                    expect(apiResponse?.count).toEventually(beLessThanOrEqualTo(0), timeout: timeout)
                    expect(networkError).toEventually(beNil(), timeout: timeout)
                }
                
                it("Should return an empty list with error: Invalid response structure") {
                    let artefactList: JSON = ["_embedded" : stubs]
                    self.stub(everything, json(artefactList))
                    var apiResponse: [Article]? = nil
                    var networkError: Error? = nil
                    
                    apiHandler.fetchArtefacts(completion: { (list, error) in
                        apiResponse = list
                        networkError = error
                    })
                    
                    expect(apiResponse).toEventuallyNot(beNil(), timeout: timeout)
                    expect(apiResponse?.count).toEventually(beLessThanOrEqualTo(0), timeout: timeout)
                    expect(networkError).toEventuallyNot(beNil(), timeout: timeout)
                }
                
                it("Should return an empty list with error: Invalid response key") {
                    let artefactList: JSON = ["_embedded" : ["wrongKey" : stubs]]
                    self.stub(everything, json(artefactList))
                    var apiResponse: [Article]? = nil
                    var networkError: Error? = nil
                    
                    apiHandler.fetchArtefacts(completion: { (list, error) in
                        apiResponse = list
                        networkError = error
                    })
                    
                    expect(apiResponse).toEventuallyNot(beNil(), timeout: timeout)
                    expect(apiResponse?.count).toEventually(beLessThanOrEqualTo(0), timeout: timeout)
                    expect(networkError).toEventuallyNot(beNil(), timeout: timeout)
                }
                
                it("Should return an error: Response is not a Json") {
                    let bodyData = "[somerandomData]".data(using: .utf8)!
                    self.stub(everything, http(download: .content(bodyData)))
                    var apiResponse: [Article]? = nil
                    var networkError: Error? = nil
                    
                    apiHandler.fetchArtefacts(completion: { (list, error) in
                        apiResponse = list
                        networkError = error
                    })
                    
                    expect(networkError).toEventuallyNot(beNil(), timeout: timeout)
                    expect(apiResponse).toEventuallyNot(beNil(), timeout: timeout)
                    expect(apiResponse?.count).toEventually(beLessThanOrEqualTo(0), timeout: timeout)
                }
                
                it("Should return an error: Returned an http error") {
                    let errorFromServer = NSError.init(domain: "apiTest", code: 503, userInfo: nil)
                    self.stub(everything, failure(errorFromServer))
                    var apiResponse: [Article]? = nil
                    var networkError: Error? = nil
                    
                    apiHandler.fetchArtefacts(completion: { (list, error) in
                        apiResponse = list
                        networkError = error
                    })
                    
                    expect(networkError).toEventuallyNot(beNil(), timeout: timeout)
                    expect(apiResponse).toEventuallyNot(beNil(), timeout: timeout)
                    expect(apiResponse?.count).toEventually(beLessThanOrEqualTo(0), timeout: timeout)
                }
            }
        }
    }
    
}
